import React, { Component, useState, useEffect } from 'react';
import axios from 'axios'
import _ from 'lodash'
import { Form, Button } from 'antd'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { useHistory, Switch, Route, Link } from "react-router-dom"
import * as actions from './reduxe/actions'
import About from './components/About'
import Help from './components/Help'
import Home from './components/home'
import MyForm from './components/myForm'
import './App.less';
const io = axios.create({
    withCredentials: true
})

io.interceptors.request.use(config => {
    console.log('intercept config', config)
    
    return config
})

io.interceptors.response.use(res => {
    console.log('intercept res', res)
    return res
})

const url1 = 'http://localhost:3000/test'
const url2 = 'http://localhost:3000/getText'
const bisheng = 'http://10.85.128.89:8000/biz-fe/bisheng-server/gulfstream/bisheng/openapi/template/version/latest?key=A6o6IV-Zd_billboard'

const App  = (props) => {
    const [template, setTemplate] = useState({})
    const { form } = props
    const url = '/api/webapp/appidManager/getAllTemplateInfo'

    useEffect(() => {
        window.onerror = (message, url, line) => {
            console.log('message', message)
            console.log('url', url)
            console.log('line', line)
            return false
        }
    }, [])
    const getCors = (url) => {
        io.get(url, { params: { name: 'jackgaoyuan' } })
            .then(res => {
                console.log('res', res)
            })
            .catch(err => {
                console.log('err', err)
            })
    }

    const postCors = (url) => {
        io.post(url)
            .then(res => {
                console.log('res', res.data)
            })
            .catch(err => {
                console.log('err', err)
            })
    }
    const versionTest = (version) => {
        io.get(bisheng)
            .then(res => {
                console.log('res', res.data)
            })
            .catch(err => {
                console.log('err', err)
            })
    }

    const errorHandler = () => {
        const str = 'fdfdf';
        str.sort()
        try {
        } catch(e) {
            console.log(e)
        }
    }

    return (
        <div>
            <About />
            <Button onClick={errorHandler}>Error handler</Button>
            <div className='test'>
            </div>
        </div>
    )
}

export default App
