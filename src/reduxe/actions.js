import * as actionTypes from './actionTypes'

export const add = number => {
    return {
        type: actionTypes.ADD,
        payload: number
    }
}

export const reduce = number => {
    return {
        type: actionTypes.REDUCE,
        payload: number
    }
}

export const multiple = number => {
    return (dispatch) => {
        const factor = 3
        dispatch(() => {
            const multi = 2
            dispatch({
                type: actionTypes.MULTIPLE,
                payload: number * factor * multi
            })
        })
    }
}


