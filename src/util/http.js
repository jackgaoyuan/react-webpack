import axios from 'axios';
import _ from 'lodash';

const io = axios.create({
  baseURL: '',
  timeout: 30 * 1000,
  withCredentials: true,
});

const handleResponse = (responseData) => {
  return responseData;
};

io.interceptors.request.use((config) => {
  return config;
}, (err) => {
  return Promise.reject(err);
});

io.interceptors.response.use((res) => {
  const { data } = res;
  return handleResponse(data);
}, (err) => {
  return Promise.reject(err);
});

const httpClient = {};
_.each(['get', 'post', 'put', 'delete'], (method) => {
  httpClient[method] = (...args) => {
    const url = args[0];
    const params = args[1];
    return io[method](url, params)
      .then((data) => {
        return data;
      })
      .catch((err) => {
        // 业务逻辑 的错误，如 errorCode 不符合预期
        // 进行参数化，把业务的error放到跟data同级的参数里，而非异常
        if (_.isUndefined(err.responseData)) {
          throw err;
        }
        return { err };
      });
  };
});

export default httpClient;
export { io };
