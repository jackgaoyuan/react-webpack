import * as actionTypes from '../actionTypes'

const initState = {
    list: 1
}

export function layoutReducer(state = initState, action) {
    switch(action.type) {
        case actionTypes.ADD_LIST: {
            const { payload } = action
            const { list } = state
            return {
                ...state,
                ... { list: list + payload }
            }
        }
        case actionTypes.REDUCE: {
            const { payload } = action
            const { list } = state
            return {
                ...state,
                ... { list: list - payload }
            }
        }
        case actionTypes.MULTIPLE: {
            const { payload } = action
            const { list } = state
            return {
                ...state,
                ... {list: list * payload}
            }
        }
        default: {
            return state
        }
    }
}
