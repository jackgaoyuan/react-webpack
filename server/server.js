const Koa = require('koa');
const router = require('koa-router')();
const path = require('path')
const cors = require('koa2-cors')
const koaBody = require('koa-body');
const static = require('koa-static')
const multer = require('@koa/multer');
const fileController = require('./controller/uploadFile')

const upload = multer();
const app = new Koa();

// 配置静态资源服务器
app.use(static(path.resolve(__dirname, '../dist')))

// parse request body
app.use(koaBody({ multipart: true }));
// 配置 CORS
app.use(cors({
    origin: 'http://localhost:8080',
    credentials: true,
}))

// 配置路由
router.get('/test', fileController.test)

router.get('/getText', fileController.getText)

router.post('/uploadFile', fileController.uploadFile)
// route.options('/uploadFile', fileController.uploadFileOption)
router.post('/postList', fileController.postList)
// 启动路由
app.use(router.routes());
app.use(router.allowedMethods());

// 启动程序监听端口
app.listen(3000);
