import React from 'react'
import { Form, Input, Select } from 'antd'
const { Option } = Select

const MyForm = (props) => {
    const { form } = props
    const { getFieldDecorator } = form
    return (
        <div>
            <Form>
                <Form.Item>
                    {
                        getFieldDecorator('a', {
                            initialValue: 'x'
                        })(
                            <Select>
                                <Option key='x' value='x'>Option x</Option>
                                <Option key='y' value='y'>Option y</Option>
                            </Select>
                        )
                    }
                </Form.Item>
            </Form>
        </div>
    )
}

export default MyForm