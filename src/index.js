import React from 'react'
import ReactDOM from 'react-dom'
import { HashRouter as Router, Route, Link } from 'react-router-dom'
import { applyMiddleware, createStore, compose, combineReducers } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import App from './App'
import { commonReducer as common} from './reduxe/reducer/commonReducer'
import { layoutReducer as layout } from './reduxe/reducer/layoutReducer'

const store = createStore(combineReducers({common, layout}), applyMiddleware(thunk))

store.subscribe(() => console.log('store state', store.getState()))

ReactDOM.render(
    <Router>
        <Provider store={store}>
            <App />
        </Provider>,
    </Router>,
    document.getElementById('root'))