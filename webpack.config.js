
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ManifestPlugin = require('webpack-manifest-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

module.exports = {
    mode: 'development',
    devtool: 'cheap-module-source-map',
    entry: {
        index: './src/index.js'
    },

    output: {
        filename: 'js/[name].[hash:5].js',
        chunkFilename: 'js/[id].[hash:5].js',
        path: path.resolve(__dirname, 'dist'),
    },

    optimization: {
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendors',
                    chunks: 'all',
                },
            },
        },
        runtimeChunk: 'single',
        moduleIds: 'hashed',
    },

    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: ['babel-loader'],
            },
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader'],
                // use: ['style-loader', 'css-loader']
            },
            {
                test: /\.less$/,
                use: [
                    // {
                    //     loader: "style-loader" // creates style nodes from JS strings
                    // },
                    MiniCssExtractPlugin.loader, 
                    {
                        loader: "css-loader" // translates CSS into CommonJS
                    }, 
                    {
                        loader: "less-loader", // compiles Less to CSS
                    }
                ]
            },
            {
                test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
                loader: 'url-loader',
                options: {
                    name: 'img/[name].[hash:5].[ext]',
                    limit: 1000
                }
            }
        ]
    },
    // resolve: {
    //     extensions: ['*', '.js', '.jsx']
    // },
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: './src/index.html',
        }),
        new ManifestPlugin({
            fileName: 'manifest.json',
            publicPath: '',
        }),
        new CleanWebpackPlugin(),
        // new MiniCssExtractPlugin({
        //     filename: 'style/[name].[hash:5].css'
        // })
        new MiniCssExtractPlugin(),
        // new BundleAnalyzerPlugin(),
    ],

    devServer: {
        publicPath: '',
        proxy: {
            '/api': {
                target: 'http://10.179.81.184:9230',
                pathRewrite: { '/api': '' } 
            }
        }
    }
}