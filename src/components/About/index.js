import React from 'react'
import { Table } from 'antd';
import G6 from '@antv/g6';
import { Button, Form, Input, Space, Pagination } from 'antd'
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';

const About = (props) => {
    const onPageChange = () => {
        console.log('onPageChange')
    }
    return (
        <div>
            <Pagination
                total={100}
                current={1}
                pageSize={20}
                onChange={onPageChange}
            />
        </div>
    )
}

export default About