const axios = require('axios')

const uploadFile = async (ctx) => {
    console.log('res', res.response)
    console.log('files ', ctx.request.files);
    console.log('body ', ctx.request.body);
    ctx.body = {
        files: ctx.request.files,
        body: ctx.request.body
    };
}

const postList = async (ctx) => {
    console.log('postlist', ctx.request.body)
    ctx.response.body = {
        data: 'postList',
        errno: 0,
        errmsg: ''
    }
}

const test = async (ctx) => {
    // ctx.set("Access-Control-Allow-Orign", 'http://localhost:8080')
    // ctx.set("Access-Control-Allow-Credentials", true)
    ctx.cookies.set('my name', 'gao yuan')
    ctx.response.body = 'test'
}
const getText = async (ctx) => {
    // ctx.set("Access-Control-Allow-Orign", 'http://localhost:8080')
    // ctx.set("Access-Control-Allow-Credentials", true)
    ctx.response.body = 'getText'
}

module.exports = {
    uploadFile,
    postList,
    test,
    getText,
}