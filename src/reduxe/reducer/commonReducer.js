import * as actionTypes from '../actionTypes'

const initState = {
    count: 0
}

export function commonReducer(state = initState, action) {
    switch(action.type) {
        case actionTypes.ADD: {
            const { payload } = action
            const { count } = state
            return {
                ...state,
                ... { count: count + payload }
            }
        }
        case actionTypes.REDUCE: {
            const { payload } = action
            const { count } = state
            return {
                ...state,
                ... { count: count - payload }
            }
        }
        default: {
            return state
        }
    }
}
